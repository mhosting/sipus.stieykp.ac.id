# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :member do
    nim "MyString"
    kelas "MyString"
    concentration_id 1
    berlaku "2017-08-06 13:22:28"
    user_id 1
  end
end
