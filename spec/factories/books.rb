# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :book do
    judul "MyString"
    pengarang "MyString"
    tahun_terbit "MyString"
    kota_terbit "MyString"
    jumlah "MyString"
  end
end
