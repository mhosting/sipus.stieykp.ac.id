# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :task do
    title "MyString"
    author "MyString"
    force "MyString"
    concentration_id 1
    stock 1
  end
end
