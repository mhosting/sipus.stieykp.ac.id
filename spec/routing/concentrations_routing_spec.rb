require "spec_helper"

describe ConcentrationsController do
  describe "routing" do

    it "routes to #index" do
      get("/concentrations").should route_to("concentrations#index")
    end

    it "routes to #new" do
      get("/concentrations/new").should route_to("concentrations#new")
    end

    it "routes to #show" do
      get("/concentrations/1").should route_to("concentrations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/concentrations/1/edit").should route_to("concentrations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/concentrations").should route_to("concentrations#create")
    end

    it "routes to #update" do
      put("/concentrations/1").should route_to("concentrations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/concentrations/1").should route_to("concentrations#destroy", :id => "1")
    end

  end
end
