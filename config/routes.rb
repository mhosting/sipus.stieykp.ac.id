Rails3BootstrapDeviseCancan::Application.routes.draw do

  resources :transactions
  resources :members
  resources :books
  resources :concentrations
  resources :tasks

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  if Rails.env.production?
    devise_for :users, :controllers => { :registrations => "registrations" } 
  else
    devise_for :users
  end
  resources :users
end