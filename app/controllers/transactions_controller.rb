class TransactionsController < ApplicationController
  def index
    @members = Member.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @members }
    end
  end

  def show
    @member = Member.find(params[:id])
    @books = Book.where("jumlah != ?", 0)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @member }
    end
  end
end
