class Task < ActiveRecord::Base
  attr_accessible :author, :concentration_id, :force, :stock, :title
  belongs_to :concentration
end
