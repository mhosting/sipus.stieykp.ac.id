class Concentration < ActiveRecord::Base
  attr_accessible :name
  has_many :tasks
  has_many :members
end
