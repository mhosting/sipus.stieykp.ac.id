class Member < ActiveRecord::Base
  attr_accessible :berlaku, :concentration_id, :kelas, :nama, :email, :nim, :user_id
  belongs_to :concentration
  belongs_to :user
end
