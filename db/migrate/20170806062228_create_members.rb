class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :nama
      t.string :nim
      t.string :email
      t.string :kelas
      t.integer :concentration_id
      t.datetime :berlaku
      t.integer :user_id

      t.timestamps
    end
  end
end
