class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.string :author
      t.string :force
      t.integer :concentration_id
      t.integer :stock

      t.timestamps
    end
  end
end
