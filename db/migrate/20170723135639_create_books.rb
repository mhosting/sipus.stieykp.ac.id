class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :judul
      t.string :pengarang
      t.string :tahun_terbit
      t.string :kota_terbit
      t.string :jumlah
      t.integer :categori_id
      t.integer :publisher_id

      t.timestamps
    end
  end
end
